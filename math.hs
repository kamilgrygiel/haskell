module Math
where

f :: Double -> Double
f x | x < -10 = x^2
    | x < 0 && -10 <= x = sin x
    | x <= 2 && 0 <= x = sqrt x 
    
    
seq' :: Integer -> Double
seq' n | n == 1 = 3
       | n == 2 = 4
       | n > 2 = 0.5  * seq'(n-1) + 2 * seq'(n-2) 
       
       
f1 = (+ (-2))
f2 = (`mod` 5)
f3 = (== 10)
f4 = (`div` 8)
f5 = (> 100)
f6 = (5 `mod`)
f7 = (8 `div`)
f8 = (* 3)

infix 1 ><
(><) :: Int -> Int -> Bool
x >< y | gcd x y == 1 = True
       | otherwise = False


