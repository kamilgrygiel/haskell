module Complex
where 

infix 1 +.
(+.) :: (Double, Double) -> (Double, Double) -> (Double, Double)
(x,y) +. (a,b) = (x + y , a + b)

infix 1 -.
(-.) :: (Double, Double) -> (Double, Double) -> (Double, Double)
(x,y) -. (a,b) = (x - y , a - b)

infix 1 *.
(*.) :: (Double, Double) -> (Double, Double) -> (Double, Double)
(x,y) *. (a,b) = (x * y , a * b)

re :: (Double, Double) -> Double
re (a,_) = a

im :: (Double, Double) -> Double
im (_,a) = a    
