module List 
where 

import Data.Char

sumOfSquares :: (Num a) => [a] -> a
sumOfSquares [] = 0
sumOfSquares [x] = x^2
sumOfSquares (x:xs) = x^2 + (sumOfSquares xs) 

sum1 :: (Num a) => [a] -> a
sum1 [] = 0
sum1 [x] = x
sum1 (x:_:xs) = x + (sum1 xs)

sum3 :: (Num a) => [a] -> a
sum3 [] = 0
sum3 [_] = 0
sum3 [_,_] = 0
sum3 (_:_:x:xs) = x + (sum xs)


countLower :: String -> Int
countLower xs = length (filter isLower xs)
 
countLowerUpper :: String -> (Int, Int)
countLowerUpper xs = (length (filter isLower xs), length (filter isUpper xs))


--cgtx :: (Num a) => a -> [a] -> a
--cgtx a [] = 0
--cgtx a [x] | a < x = (+1)
--cgtx a (x:xs) | a < x = (+1) 

factors :: Int -> [Int]
factors n = [x | x <- [1..n], n `mod` x == 0]

prime :: Int -> Bool
prime n = factors n == [1,n]

primes :: [Int]
primes = filter prime [2 ..]

pairs :: [Int] -> [(Int, Int)]
pairs (x:y:[])  | x + 2 == y = [(x,y)]
                | otherwise  = []
pairs (x:y:xys) | x + 2 == y = (x,y) : pairs (y:xys)
                | otherwise  = pairs (y:xys)
                
primePairs :: [(Int, Int)]
primePairs = pairs primes

l1 = [x | x <- [100..999], x `mod` 2 == 1, x `mod` 17 == 0]
l2 = [x^2 | x <- [1..225], x^2 < 50000]  
l3 = [(x, 2^x) | x <- [1..1000], 2^x < 5000, 2^x > 1000]
l4 = [(x, y) | x <- ['A'..'H'], y <- [1..8]]
l5 = [(x, y) | x <- [1..9], y <- [1..9], x*y < 10]
l6 = [(x, y, z) | x <- [1..9], y <- [1..9], z <- [1..9], x /= y, x*y < z]
l7 = [[(x*y)| x <- [1..10]] | y <- [1..10]]


s1 = sum[1/n | n <- [1..100]]
s2 = sum[1/(n^2) | n <- [1..1000]]
s3 = sum[(sqrt n) - 1/n | n <- [1..1000]]
p1 = product[(1+n) / (2+n) | n <- [1..50]]
p2 = product[(n+1) / (sqrt n) | n <- [1..10]]
p3 = (product[1,2,4,6])**(1/fromIntegral(length [1,2,4,6]))
