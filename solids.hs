module Solids
where

sphereVolume :: Double -> Double
sphereVolume r = 4/3 * pi * r^3

coneVolume :: Double -> Double -> Double
coneVolume r h = 1/3 * pi * r^2 * h

cuboidVolume :: Double -> Double -> Double -> Double
cuboidVolume a b c = a * b * c
